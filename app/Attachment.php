<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Attachment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'attachment';
    protected $fillable = [
        'name',
        'attachment',
        'mime'
    ];
}
