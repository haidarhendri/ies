<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HaveTagsContact extends Model
{
    protected $table = 'haveTagsContact';
    protected $fillable = [
        'idContact',
        'tagName'
    ];
}
