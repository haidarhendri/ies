<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\HaveTags;

class HaveTagsTransformer extends TransformerAbstract
{
    public function transform(HaveTags $haveTags)
    {
        return [
            'idCompany' => $haveTags->idCompany,
            'tagName' => $haveTags->tagName
        ];
    }
}
