<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Contact;

class ContactTransformer extends TransformerAbstract
{
    public function transform(Contact $contact)
    {
        return [
            'id' => $contact->id,
            'title' => $contact->title,
            'firstName' => $contact->firstName,
            'lastName' => $contact->lastName,
            'email' => $contact->email,
            'phone' => $contact->phone,
            'handPhone' => $contact->handPhone,
            'jobTitle' => $contact->jobTitle,
            'idCompany' => $contact->idCompany,
            'idAddress' => $contact->idAddress,
            'website' => $contact->website,
            'birthday' => $contact->birthday,
            'accountManager' => $contact->accountManager,
            'picture' => $contact->picture,
            'customData' => $contact->customData
        ];
    }
}
