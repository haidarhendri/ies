<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Attachment;

class AttachmentTransformer extends TransformerAbstract
{
    public function transform(Attachment $attachment)
    {
        return [
            'id' => $attachment->id,
            'name' => $attachment->name,
            'attachment' => $attachment->attachment,
            'mime' => $attachment->mime
        ];
    }
}
