<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Segment;

class SegmentTransformer extends TransformerAbstract
{
    public function transform(Segment $segment)
    {
        return [
            'id' => $segment->id,
            'name' => $segment->name,
            'alias' => $segment->alias,
            'description' => $segment->description,
            'filter' => $segment->filter
        ];
    }
}
