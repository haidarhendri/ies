<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\HaveSegment;

class HaveSegmentTransformer extends TransformerAbstract
{
    public function transform(HaveSegment $haveSegment)
    {
        return [
            'id' => $haveSegment->id,
            'idContact' => $haveSegment->idContact,
            'idSegment' => $haveSegment->idSegment
        ];
    }
}
