<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Company;

class CompanyTransformer extends TransformerAbstract
{
    public function transform(Company $company)
    {
        return [
            'id' => $company->id,
            'name' => $company->name,
            'website' => $company->website,
            'idAddress' => $company->idAddress,
            'phone' => $company->phone,
            'email' => $company->email,
            'accountManager' => $company->accountManager,
            'picture' => $company->picture
        ];
    }
}
