<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Email;

class EmailTransformer extends TransformerAbstract
{
    public function transform(Email $email)
    {
        return [
            'id' => $email->id,
            'subject' => $email->subject,
            'body' => $email->body,
            'senderName' => $email->senderName,
            'senderEmail' => $email->senderEmail,
            'replyToEmail' => $email->replyToEmail
        ];
    }
}
