<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $userApi)
    {
        return [
            'id' => $userApi->id,
            'name' => $userApi->name,
            'email' => $userApi->email,
            'address' => $userApi->idAddress,
            'phone' => $userApi->phone,
            'picture' => $userApi->picture
        ];
    }
}
