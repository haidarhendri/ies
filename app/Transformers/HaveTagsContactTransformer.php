<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\HaveTagsContact;

class HaveTagsContactTransformer extends TransformerAbstract
{
    public function transform(HaveTagsContact $haveTagsContact)
    {
        return [
            'idContact' => $haveTagsContact->idCompany,
            'tagName' => $haveTagsContact->tagName
        ];
    }
}
