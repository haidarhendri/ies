<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Address;

class AddressTransformer extends TransformerAbstract
{
    public function transform(Address $address)
    {
        return [[
            'id' => $address->id,
            'street' => $address->street,
            'zip' => $address->zip, 
            'city' => $address->city,
            'country' => $address->country
        ]];
    }
}
