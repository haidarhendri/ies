<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\ContactTags;

class ContactTagsTransformer extends TransformerAbstract
{
    public function transform(ContactTags $contactTags)
    {
        return [
            'name' => $contactTags->name
        ];
    }
}
