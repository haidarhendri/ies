<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JavaScript;

class AppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * App Request.
     */
    public function commit()
    {
        JavaScript::put([
            'logoDirectory' => env('IMAGE_ASSET_DIRECTORY'),
            'appName' => env('APP_NAME'),
            'scssDirectory' => env('SCSS_ASSET_DIRECTORY'),
            'sentry' => env('SENTRY_LARAVEL_DSN')
        ]);

        return view('index');
    }
}
