<?php

namespace App\Http\Requests\API\ContactTags;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use App\ContactTags;
use App\Transformers\ContactTagsTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        return fractal(ContactTags::all(), ContactTagsTransformer::class)->toArray()['data'];
    }
}
