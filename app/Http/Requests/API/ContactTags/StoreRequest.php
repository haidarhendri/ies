<?php

namespace App\Http\Requests\API\ContactTags;

use Illuminate\Foundation\Http\FormRequest;
use App\ContactTags;
use App\Transformers\ContactTagsTransformer;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(ContactTags::create([
          'name' => $this->name]),
          ContactTagsTransformer::class)->toArray()['data'];
    }

    public function getTagName()
    {
        return ContactTags::where('name', $this->name)->get();
    }
}
