<?php

namespace App\Http\Requests\API\Email;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use App\{Email, Attachment, HaveAttachment, Segment, HaveSegment, HaveSegmentEmail};
use App\Transformers\{EmailTransformer, AttachmentTransformer};
use App\Jobs\SendEmailJob;
use Carbon\Carbon;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }

    function storeEmail()
    {
        if(isset($this->deleteStack) && sizeof($this->deleteStack) > 0)
        {
            foreach($this->deleteStack as $del)
            {
                HaveAttachment::where('idAttachment', $del)
                ->delete();
            }
        }

        $toFractal = fractal(Email::updateOrCreate([
            'id' => $this->id
        ],[
            'subject' => $this->subject,
            'body' => $this->body,
            'replyToEmail' => $this->replyToEmail]),
            EmailTransformer::class)->toArray()['data'];

        $a = (object) $toFractal;

        if ($this->file('attachment') != null)
        {
            $storeAttachment = [];
            $storeHaveAttachment = [];
            
            foreach($this->file('attachment') as $i=>$value)
            {
                $att[] = 
                fractal(Attachment::firstOrCreate([
                    'name' => $value->getClientOriginalName(),
                    'attachment' => $value->store('public/files'),
                    'mime' => $value->getMimeType()
                ]),
                AttachmentTransformer::class)->toArray()['data'];
                array_push($storeAttachment,$att[$i]["attachment"]);

                $idAttachment[] = Attachment::where('attachment',$storeAttachment[$i])->pluck('id');
                $storeHaveAttachment[] = 
                HaveAttachment::firstOrCreate([
                    'idEmail' => $a->id,
                    'idAttachment' => $idAttachment[$i][0]
                ]);
            }
        }
        else
        {
            $storeAttachment = [];
            $storeHaveAttachment = [];
        }

        $toFractal = $this->array_push_assoc($toFractal,'attachment',$storeAttachment);

        return $toFractal;
    }

    function generateContact($afterDecode)
    {
        $query = "SELECT contact.* FROM (contact, address, haveTagsContact, company)";
        $conditions = [];

        foreach($afterDecode as $filter)
        {
            if($filter['name'] == 'Tags')
            {
                if($filter['option'] == 'include')
                {
                    $temp = "";
                    foreach($filter['filterValue'] as $key => $tag)
                    {
                        if($key < (count($filter['filterValue'])-1))
                        {
                            $temp = $temp."haveTagsContact.tagName = '".$tag."' OR ";
                        }
                        else
                        {
                            $temp = $temp."haveTagsContact.tagName = '".$tag."'";
                        }                        
                    };
                    $temp = $filter['conjunction']. " contact.id = haveTagsContact.idContact AND (".$temp.")";
                    $conditions[] = $temp;
                }
                else if($filter['option'] == 'exclude')
                {
                    $temp = "";
                    foreach($filter['filterValue'] as $key => $tag)
                    {
                        if($key < (count($filter['filterValue'])-1))
                        {
                            $temp = $temp."haveTagsContact.tagName != '".$tag."' AND ";
                        }
                        else
                        {
                            $temp = $temp."haveTagsContact.tagName != '".$tag."'";
                        }                        
                    };
                    $temp = $filter['conjunction']. " contact.id = haveTagsContact.idContact AND (".$temp.") ";
                    $conditions[] = $temp;
                }
            }
            else if($filter['name'] == 'Country')
            {
                if($filter['option'] == 'include')
                {
                    $conditions[] = $filter['conjunction']. " contact.idAddress = Address.id AND (address.country = '". $filter['filterValue'] ."')";
                }
                else if($filter['option'] == 'exclude')
                {
                    $conditions[] = $filter['conjunction']. " contact.idAddress = Address.id AND (address.country != '". $filter['filterValue'] ."')";
                }
            }
            else if($filter['name'] == 'City')
            {
                if($filter['option'] == 'equals')
                {
                    $conditions[] = $filter['conjunction']. " contact.idAddress = Address.id AND (address.city = '". $filter['filterValue'] ."')";
                }
                else if($filter['option'] == 'not equals')
                {
                    $conditions[] = $filter['conjunction']. " contact.idAddress = Address.id AND (address.city != '". $filter['filterValue'] ."')";
                }
            }
            else if($filter['name'] == 'Company')
            {
                if($filter['option'] == 'equals')
                {
                    $conditions[] = $filter['conjunction']. " contact.idCompany = company.id AND (company.name = '". $filter['filterValue'] ."')";
                }
                else if($filter['option'] == 'not equals')
                {
                    $conditions[] = $filter['conjunction']. " contact.idCompany = company.id AND (company.name != '". $filter['filterValue'] ."')";
                }
            }
        };

        $conditions[0] = substr($conditions[0], 4);

        if (count($conditions) > 0) {
            $query .= " WHERE " . implode('', $conditions);
          }
        
        $getContact = DB::select(DB::raw($query));
         
        return $getContact;
    }

    public function saveSegmentEmail()
    {
        $getEmail = $this->storeEmail();

        $contactEmail = [];

        HaveSegmentEmail::where('idEmail', $getEmail['id'])
            ->delete();

        if($this->segment != [])
        {
            foreach($this->segment as $key => $i)
            {
                $i = $i*1;
                $getFilter = DB::select(DB::raw(
                    "SELECT * FROM `segment` WHERE id = ".$i));
                $afterDecode = json_decode(($getFilter[0]->filter),true);
    
                $dapatContact = $this->generateContact($afterDecode);
        
                HaveSegment::where('idSegment', $i)
                    ->delete();
                
                if($dapatContact != [])
                {
                    foreach($dapatContact as $contact)
                    {
                        $storeHaveSegment[] = 
                        HaveSegment::firstOrCreate([
                            'idContact' => $contact->id,
                            'idSegment' => $i
                        ]);
                        $segmentName[] = Segment::where('id',$i)->pluck('name');
                        $contactEmail[] = $contact->email;
                    }
                }
            }
            foreach($this->segment as $i)
            {
                $storeHaveSegmentEmail[] = 
                HaveSegmentEmail::firstOrCreate([
                    'idEmail' => $getEmail['id'],
                    'idSegment' => $i
                ]);
            }
        }
        else
        {
            $segmentName = [];
            $storeHaveSegment = [];
            $storeHaveSegmentEmail = [];
        }

        $contactEmailResult = [$contactEmail, $getEmail];
        return $contactEmailResult;
    }

    public function commit()
    {   
        $sendEmail = $this->storeEmail($this->file('attachment'), $this->deleteStack);
        $toEmail = "haidarhendri7@gmail.com";

        $emailJob = (new SendEmailJob(
            $this->subject,
            $this->body,
            $toEmail,
            $this->replyToEmail,
            $this->attachment
            ))->delay(Carbon::now()->addSeconds(3));
        dispatch($emailJob);

        return "Email Berhasil Terkirim";
    }

    public function sendToSegment()
    {
        $contactEmail = $this->saveSegmentEmail();

        foreach(array_unique($contactEmail[0]) as $sendEmail)
        {
            $emailJob = (new SendEmailJob(
                $this->subject,
                $this->body,
                $sendEmail,
                $this->replyToEmail,
                $contactEmail[1]['attachment']
                ))->delay(Carbon::now()->addSeconds(3));
            dispatch($emailJob);
        }

        return 'Email Berhasil Terkirim ke Segment';
    }

    public function sendToContact()
    {
        $this->idContact = 3;
        $this->subject = "coba ke contact";
        $this->body = "test";
        $this->replyToEmail = "haidarhendri@student.uns.ac.id";


        foreach(array_unique($contactEmail) as $sendEmail)
        {
            $emailJob = (new SendEmailJob(
                $this->subject,
                $this->body,
                $sendEmail,
                $this->replyToEmail,
                $this->attachment
                ))->delay(Carbon::now()->addSeconds(3));
            dispatch($emailJob);
        }

        return 'Email Berhasil Terkirim ke Kontak';
    }

    public function getId()
    {
        return Email::where('id', $this->id)->get()->map(function($item)
        {
            return [
                'id' => $item->id,
                'subject' => $item->subject,
                'body' => $item->body,
                'replyToEmail' => $item->replyToEmail,
                'attachment' => HaveAttachment::with('attachment')->where('idEmail', $item->id)->get(),
                'segment' => HaveSegmentEmail::with('segmentEmail')->where('idEmail', $item->id)->get(),
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
                'deleted_at' => $item->deleted_at
            ];
        });
    }

    public function destroy()
    {
        $attachmentEmail = HaveAttachment::where('idEmail', $this->id)->delete();
        $segmentEmail = HaveSegmentEmail::where('idEmail', $this->id)->delete();
        $emailApi = Email::where('id',$this->id)->delete();

        return 'success';
    }
}
