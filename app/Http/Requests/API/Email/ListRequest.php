<?php

namespace App\Http\Requests\API\Email;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use App\{Email, HaveAttachment, HaveSegmentEmail};
use App\Transformers\EmailTransformer;

class ListRequest extends FormRequest
{
    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        return Email::all()->map(function($item){
            return [
                'id' => $item->id,
                'subject' => $item->subject,
                'body' => $item->body,
                'senderName' => $item->senderName,
                'senderEmail' => $item->senderEmail,
                'replyToEmail' => $item->replyToEmail,
                'attachment' => HaveAttachment::with('attachment')->where('idEmail', $item->id)->get(),
                'segment' => HaveSegmentEmail::with('segmentEmail')->where('idEmail', $item->id)->get(),
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
                'deleted_at' => $item->deleted_at
            ];
        });
    }

    public function getSegmentEmail()
    {
        $getListBySegment = Email::all()->map(function($item){
            if(count(HaveSegmentEmail::with('segmentEmail')->where('idEmail', $item->id)->get()))
            {
                return [
                    'id' => $item->id,
                    'subject' => $item->subject,
                    'body' => $item->body,
                    'senderName' => $item->senderName,
                    'senderEmail' => $item->senderEmail,
                    'replyToEmail' => $item->replyToEmail,
                    'attachment' => HaveAttachment::with('attachment')->where('idEmail', $item->id)->get(),
                    'segment' => HaveSegmentEmail::with('segmentEmail')->where('idEmail', $item->id)->get(),
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                    'deleted_at' => $item->deleted_at
                ];
            }
        });

        $temp = [];
        foreach($getListBySegment as $v){
            if($v != null){
                $temp[] = $v;
            }
        }
        return $temp;
    }
}