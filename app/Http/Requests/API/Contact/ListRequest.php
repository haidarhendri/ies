<?php

namespace App\Http\Requests\API\Contact;

use Illuminate\Foundation\Http\FormRequest;
use App\{Contact, HaveSegment};
use Illuminate\Support\Facades\DB;
use App\Transformers\ContactTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        return Contact::with('address')->with('segment')->get()->map(function($item){
            date_default_timezone_set('Asia/Jakarta');
            $getData = date("Y-m-d H:i:s",intval($item->birthday));
            return [
                'id' => $item->id,
                'title' => $item->title,
                'firstName' => $item->firstName,
                'lastName' => $item->lastName,
                'email' => $item->email,
                'phone' => $item->phone,
                'handPhone' => $item->handPhone,
                'jobTitle' => $item->jobTitle,
                'idCompany' => $item->idCompany,
                'idAddress' => $item->address,
                'website' => $item->website,
                'birthday' => $getData,
                'accountManager' => $item->accountManager,
                'picture' => base64_encode($item->picture),
                'customData' => $item->customData,
                'tags' => DB::table('haveTagsContact')
                    ->where('idContact', $item->id)
                    ->get(),
                'segment' => HaveSegment::with('segment')->where('idContact', $item->id)->get()->map(function($value){
                    return $value->segment;
                }),
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at
            ];
        });
    }
}
