<?php

namespace App\Http\Requests\API\Contact;

use Illuminate\Foundation\Http\FormRequest;
use App\{Contact,HaveTagsContact,Tag,Address,HaveSegment, Segment};
use App\Transformers\{AddressTransformer,ContactTransformer};

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            //
        ];
    }

    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }

    public function commit()
    {
        if($this->street != "" && $this->zip != "" && $this->city != "" && $this->country != ""){
            $storeAddr = fractal(Address::firstOrCreate([
                'street' => $this->street,
                'zip' => $this->zip,
                'city' => $this->city,
                'country' => $this->country]),
                AddressTransformer::class)->toArray()['data'];
            }
        else
        {
            $storeAddr = [];
            array_push($storeAddr,['id' => null]); 
        }

        $idAddr = (object) $storeAddr[0];

        if(sizeof($this->tags) > 0){
            foreach($this->tags as $i){
                $storeTag[] = 
                Tag::firstOrCreate(['name' => $i]);
            }
        }
        else
        {
            $storeTag = [];
        }

        $toFractal = fractal(Contact::updateOrCreate([
            'id' => $this->id,
        ],[
            'title' => $this->title,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'phone' => $this->phone,
            'handPhone' => $this->handPhone,
            'jobTitle' => $this->jobTitle,
            'idCompany' => $this->idCompany,
            'idAddress' => $idAddr->id,
            'website' => $this->website,
            'birthday' => $this->birthday,
            'accountManager' => $this->accountManager,
            'picture' => $this->picture,
            'customData' => $this->customData,
            ]),
            ContactTransformer::class)->toArray()['data'];

        $a = (object) $toFractal;

        HaveTagsContact::where('idContact', $a->id)
            ->delete();

        if(sizeof($this->tags) > 0){
            foreach($this->tags as $i){
                $storeHaveTagsContact[] = 
                HaveTagsContact::firstOrCreate([
                    'idContact' => $a->id,
                    'tagName' => $i
                ]);
            }
        }
        else
        {
            $storeHaveTagsContact = [];
        }

        HaveSegment::where('idContact', $a->id)
            ->delete();
        
        if($this->segment != []){
            foreach($this->segment as $i){
                $storeHaveSegment[] = 
                HaveSegment::firstOrCreate([
                    'idContact' => $a->id,
                    'idSegment' => $i
                ]);
                $segmentName[] = Segment::where('id',$i)->pluck('name');
            }
        }
        else
        {
            $segmentName = [];
            $storeHaveSegment = [];
        }

        $toFractal = $this->array_push_assoc($toFractal,'tags',$storeTag);
        $result = $this->array_push_assoc($toFractal,'segment',$segmentName);

        return $result;
    }

    public function getId()
    {
        return Contact::where('id',$this->id)->get();
    }

    public function destroy()
    {
        $tagContact = HaveTagsContact::where('idContact',$this->id)->delete();
        $ContactApi = Contact::where('id',$this->id)->delete();

        return 'success';
    }
}
