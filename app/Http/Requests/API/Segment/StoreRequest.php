<?php

namespace App\Http\Requests\API\Segment;

use Illuminate\Foundation\Http\FormRequest;
use App\Segment;
use App\Transformers\SegmentTransformer;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        return fractal(Segment::updateOrCreate([
            'id' => $this->id
        ],[
            'name' => $this->name,
            'alias' => $this->alias,
            'description' => $this->description,
            'filter' => $this->filter]),
            SegmentTransformer::class)->toArray()['data'];

    }

    public function getId()
    {
        return Segment::where('id',$this->id)->get();
    }

    public function destroy()
    {
        return Segment::where('id',$this->id)->delete();
    }
}
