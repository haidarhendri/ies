<?php

namespace App\Http\Requests\API\Segment;

use Illuminate\Foundation\Http\FormRequest;
use App\Segment;
use App\Transformers\SegmentTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(Segment::all(), SegmentTransformer::class)->toArray()['data'];
    }
}
