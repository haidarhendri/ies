<?php

namespace App\Http\Requests\API\HaveTagsContact;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use App\HaveTagsContact;
use App\Transformers\HaveTagsContactTransformer;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        HaveTagsContact::where('idCompany', $this->idCompany)
        ->delete();

        return fractal(HaveTagsContact::firstOrCreate([
            'idCompany' => $this->id,
            'tagName' => $this->tags]),
            HaveTagsContactTransformer::class)->toArray()['data'];
    }

    public function getData()
    {
        return HaveTagsContact::where('idCompany', $this->idCompany)
        ->get();
    }
}
