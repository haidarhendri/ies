<?php

namespace App\Http\Requests\API\HaveTagsContact;

use Illuminate\Foundation\Http\FormRequest;
use App\HaveTagsContact;
use App\Transformers\HaveTagsContactTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(HaveTagsContact::all(), HaveTagsContactTransformer::class)->toArray()['data'];
    }
}
