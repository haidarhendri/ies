<?php

namespace App\Http\Requests\API\Tag;

use Illuminate\Foundation\Http\FormRequest;
use App\Tag;
use App\Transformers\TagTransformer;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(Tag::create([
          'name' => $this->name]),
          TagTransformer::class)->toArray()['data'];
    }

    public function getTagName()
    {
        return Tag::where('name', $this->name)->get();
    }
}
