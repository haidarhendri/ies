<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\{User,Address,Company};
use App\Transformers\{UserTransformer,AddressTransformer};

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        if($this->street != "" && $this->zip != "" && $this->city != "" && $this->country != ""){
            $storeAddr = fractal(Address::firstOrCreate([
                'street' => $this->street,
                'zip' => $this->zip,
                'city' => $this->city,
                'country' => $this->country]),
                AddressTransformer::class)->toArray()['data'];
            }
        else
        {
            $storeAddr = [];
            array_push($storeAddr,['id' => null]); 
        }
        
        $idAddr = (object) $storeAddr[0];
            
        return fractal(User::updateOrCreate([
            'id' => $this->id,
        ],[
            'name' => $this->name, 
            'email' => $this->email, 
            'idAddress' => $idAddr->id, 
            'phone' => $this->phone, 
            'picture' => $this->picture
        ]), 
        UserTransformer::class)->toArray()['data'];
    }

    public function getId()
    {
        return User::where('id',$this->id)->get();
    }

    public function destroy()
    {
        $getAM = Company::select('accountManager')
        ->where('accountManager',$this->id)
        ->get();

        if(count($getAM) == 0)
        {
            User::where('id',$this->id)->delete();
        }
        else
        {
            Company::where('accountManager',$this->id)
            ->update(['accountManager' => null]);
        }

        return 'sukses';
    }
}
