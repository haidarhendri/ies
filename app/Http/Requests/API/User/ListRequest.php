<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Transformers\UserTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        return User::with('address')->get()->map(function($item){
            return [
                'id' => $item->id,
                'name' => $item->name,
                'email' => $item->email,
                'idAddress' => $item->address,
                'phone' => $item->phone,
                'picture' => base64_encode($item->picture),
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at
            ];
        });
    }
}
