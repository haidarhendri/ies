<?php

namespace App\Http\Requests\API\HaveTags;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use App\HaveTags;
use App\Transformers\HaveTagsTransformer;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        HaveTags::where('idCompany', $this->idCompany)
        ->delete();

        return fractal(HaveTags::firstOrCreate([
            'idCompany' => $this->id,
            'tagName' => $this->tags]),
            HaveTagsTransformer::class)->toArray()['data'];
    }

    public function getData()
    {
        return HaveTags::where('idCompany', $this->idCompany)
        ->get();
    }
}
