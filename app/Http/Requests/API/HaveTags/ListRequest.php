<?php

namespace App\Http\Requests\API\HaveTags;

use Illuminate\Foundation\Http\FormRequest;
use App\HaveTags;
use App\Transformers\HaveTagsTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(HaveTags::all(), HaveTagsTransformer::class)->toArray()['data'];
    }
}
