<?php

namespace App\Http\Requests\API\Company;

use Illuminate\Foundation\Http\FormRequest;
use App\{Company,HaveTags,Tag,Address};
use App\Transformers\{AddressTransformer,CompanyTransformer};

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            //
        ];
    }

    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }

    public function commit()
    {
        if($this->street != "" && $this->zip != "" && $this->city != "" && $this->country != ""){
            $storeAddr = fractal(Address::firstOrCreate([
                'street' => $this->street,
                'zip' => $this->zip,
                'city' => $this->city,
                'country' => $this->country]),
                AddressTransformer::class)->toArray()['data'];
            }
        else
        {
            $storeAddr = [];
            array_push($storeAddr,['id' => null]); 
        }

        $idAddr = (object) $storeAddr[0];
        
        $toFractal = fractal(Company::updateOrCreate([
            'id' => $this->id,
        ],[
            'name' => $this->name,
            'website' => $this->website,
            'idAddress' => $idAddr->id,
            'phone' => $this->phone,
            'email' => $this->email,
            'accountManager' => $this->accountManager,
            'picture' => $this->picture]),
            CompanyTransformer::class)->toArray()['data'];

        $a = (object) $toFractal;

        HaveTags::where('idCompany', $a->id)
            ->delete();

        if(sizeof($this->tags) > 0)
        {
            foreach($this->tags as $i)
            {
                $storeTag[] = 
                    Tag::firstOrCreate(['name' => $i]);
            }
            foreach($this->tags as $i)
            {
                $storeHaveTags[] = 
                HaveTags::firstOrCreate([
                    'idCompany' => $a->id,
                    'tagName' => $i
                ]);
            }
        }
        else
        {
            $storeTag = [];
            $storeHaveTags = [];
        }

        $toFractal = $this->array_push_assoc($toFractal,'tags',$storeTag);
        
        return $toFractal;
    }

    public function getId()
    {
        return Company::where('id',$this->id)->get();
    }

    public function destroy()
    {
        $tagCompany = HaveTags::where('idCompany',$this->id)->delete();
        $companyApi = Company::where('id',$this->id)->delete();

        return 'success';
    }
}
