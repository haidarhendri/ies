<?php

namespace App\Http\Requests\API\Company;

use Illuminate\Foundation\Http\FormRequest;
use App\Company;
use Illuminate\Support\Facades\DB;
use App\Transformers\CompanyTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
        return Company::with('address')->get()->map(function($item){
            return [
                'id' => $item->id,
                'name' => $item->name,
                'website' => $item->website,
                'idAddress' => $item->address,
                'phone' => $item->phone,
                'email' => $item->email,
                'accountManager' => $item->accountManager,
                'picture' => base64_encode($item->picture),
                'tags' => DB::table('haveTags')
                    ->where('idCompany', $item->id)
                    ->get(),
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at
            ];
        });
    }
}
