<?php

namespace App\Http\Requests\API\Address;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use App\Address;
use App\Transformers\AddressTransformer;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(Address::create([
          'street' => $this->street,
          'zip' => $this->zip,
          'city' => $this->city,
          'country' => $this->country]),
          AddressTransformer::class)->toArray()['data'];
    }

    public function getId()
    {
        return Address::where('street', $this->street)
        ->where('city', $this->city)
        ->where('country', $this->country)
        ->where('zip', $this->zip)
        ->get();
    }
}
