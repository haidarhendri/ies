<?php

namespace App\Http\Requests\API\Address;

use Illuminate\Foundation\Http\FormRequest;
use App\Address;
use App\Transformers\AddressTransformer;

class ListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function commit()
    {
      return fractal(Address::all(), AddressTransformer::class)->toArray()['data'];
    }
}
