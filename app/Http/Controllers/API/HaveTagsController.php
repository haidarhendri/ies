<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Tag;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\HaveTags\{StoreRequest, ListRequest};

class HaveTagsController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getData(StoreRequest $request)
    {
        return $request->getData();
    }
}