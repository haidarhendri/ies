<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Attachment;
use App\Http\Controllers\Controller;

class AttachmentController extends Controller
{
    public function store(Request $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getId(StoreRequest $request)
    {
        return $request->getId();
    }

    public function destroy(StoreRequest $request)
    {
        return $request->destroy();
    }
}
