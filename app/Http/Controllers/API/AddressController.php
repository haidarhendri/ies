<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Address\{StoreRequest, ListRequest};

class AddressController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getId(StoreRequest $request)
    {
        return $request->getId();
    }
}
