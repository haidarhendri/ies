<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Segment\{StoreRequest, ListRequest};

class SegmentController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getId(StoreRequest $request)
    {
        return $request->getId();
    }

    public function destroy(StoreRequest $request)
    {
        return $request->destroy();
    }
}
