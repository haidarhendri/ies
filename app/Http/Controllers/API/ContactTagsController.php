<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\ContactTags\{StoreRequest, ListRequest};

class ContactTagsController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getTagName(StoreRequest $request)
    {
        return $request->getTagName();
    }
}
