<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HaveAttachmentController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getId(StoreRequest $request)
    {
        return $request->getId();
    }

    public function destroy(StoreRequest $request)
    {
        return $request->destroy();
    }
}
