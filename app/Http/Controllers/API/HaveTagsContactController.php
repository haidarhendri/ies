<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\HaveTagsContact\{StoreRequest, ListRequest};

class HaveTagsContactController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getData(StoreRequest $request)
    {
        return $request->getData();
    }
}
