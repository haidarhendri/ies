<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Email\{StoreRequest, ListRequest};
use App\{Email, Attachment, HaveAttachment};
use App\Http\Controllers\Controller;
use App\Transformers\{EmailTransformer, AttachmentTransformer};

class EmailController extends Controller
{
    public function store(StoreRequest $request)
    {
        return $request->commit();
    }

    public function sendToSegment(StoreRequest $request)
    {
        return $request->sendToSegment();
    }

    public function saveSegmentEmail(StoreRequest $request)
    {
        return $request->saveSegmentEmail();
    }

    public function sendToContact(StoreRequest $request)
    {
        return $request->sendToContact();
    }

    public function getId(StoreRequest $request)
    {
        return $request->getId();
    }

    public function destroy(StoreRequest $request)
    {
        return $request->destroy();
    }

    public function list(ListRequest $request)
    {
        return $request->commit();
    }

    public function getSegmentEmail(ListRequest $request)
    {
        return $request->getSegmentEmail();
    }
}
