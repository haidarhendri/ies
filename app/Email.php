<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Email extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'email';
    protected $fillable = [
        'subject',
        'body',
        'replyToEmail'
    ];
}
