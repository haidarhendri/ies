<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\SendMailable;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $subject;
    public $body;
    // private $senderName;
    // private $senderEmail;
    private $toEmail;
    private $replyToEmail;
    private $attachment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subject, $body, $toEmail, $replyToEmail, $attachment)
    {
        $this->subject = $subject;
        $this->body = $body;
        // $this->senderName = $senderName;
        // $this->senderEmail = $senderEmail;
        $this->toEmail = $toEmail;
        $this->replyToEmail = $replyToEmail;
        $this->attachment = $attachment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        Mail::send(new SendMailable(
            $this->subject,
            $this->body,
            // $this->senderName,
            // $this->senderEmail,
            $this->toEmail,
            $this->replyToEmail,
            $this->attachment
        ));
    }
}