<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HaveTags extends Model
{
    protected $table = 'haveTags';
    protected $fillable = [
        'idCompany',
        'tagName'
    ];
}
