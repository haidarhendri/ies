<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class HaveAttachment extends Model
{
    protected $table = 'haveAttachment';
    protected $fillable = [
        'idEmail',
        'idAttachment'
    ];

    public function attachment(){
        return $this->belongsTo('App\Attachment','idAttachment');
    }   
}
