<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HaveSegment extends Model
{
    protected $table = 'haveSegment';
    protected $fillable = [
        'idContact',
        'idSegment'
    ];

    public function segment(){
        return $this->belongsTo('App\Segment','idSegment');
    }   
}
