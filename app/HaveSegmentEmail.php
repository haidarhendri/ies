<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HaveSegmentEmail extends Model
{
    protected $table = 'haveSegmentEmail';
    protected $fillable = [
        'idEmail',
        'idSegment'
    ];

    public function segmentEmail()
    {
        return $this->belongsTo('App\Segment','idSegment');
    }
}
