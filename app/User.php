<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'user';
    protected $fillable = [
        'name',
        'email',
        'idAddress',
        'phone',
        'picture'
    ];

    public function address(){
        return $this->belongsTo('App\Address','idAddress');
    }
}
