<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    // private $senderName;
    // private $senderEmail;
    private $toEmail;
    private $replyToEmail;
    private $attachment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $body, $toEmail, $replyToEmail, $attachment)
    {
        $this->subject = $subject;
        $this->body = $body;
        // $this->senderName = $senderName;
        // $this->senderEmail = $senderEmail;
        $this->toEmail = $toEmail;
        $this->replyToEmail = $replyToEmail;
        $this->attachment = $attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        foreach($this->attachment as $file){
            $this->attach(storage_path('app/'. $file));
        }

        $this->view('email')
                ->subject($this->subject)
                ->to($this->toEmail)
                ->replyTo($this->replyToEmail);
    }
}
