<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'contact';
    protected $fillable = [
        'title',
        'firstName',
        'lastName',
        'email',
        'phone',
        'handPhone',
        'jobTitle',
        'idCompany',
        'idAddress',
        'website',
        'birthday',
        'accountManager',
        'picture',
        'customData'
    ];

    public function address(){
        return $this->belongsTo('App\Address','idAddress');
    }
    public function segment(){
        return $this->belongsTo('App\Segment','idSegment');
    }   
}
