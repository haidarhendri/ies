<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergeTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('haveTagsContact', function (Blueprint $table) {
            $table->dropForeign('FK_Tags');
            $table->foreign('tagName', 'FK_Tags')->references('name')->on('tag');
        });

        Schema::dropIfExists('contactTags');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('contactTags', function (Blueprint $table) {
            $table->string('name');
            $table->primary('name', 'tagName');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('haveTagsContact', function (Blueprint $table) {
            $table->dropForeign('FK_Tags');
            $table->foreign('tagName', 'FK_Tags')->references('name')->on('contactTags');      
        });
    }
}
