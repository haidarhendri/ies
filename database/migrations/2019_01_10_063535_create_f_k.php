<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFK extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->foreign('idAddress', 'address_FK')->references('id')->on('address');
        });
        Schema::table('company', function (Blueprint $table) {
            $table->foreign('idAddress', 'address_Comp_FK')->references('id')->on('address');
            $table->foreign('accountManager', 'AM_FK')->references('id')->on('user');
        });
        Schema::table('haveTags', function (Blueprint $table) {
            $table->foreign('idCompany', 'company_FK')->references('id')->on('company');
            $table->foreign('tagName', 'tags_FK')->references('name')->on('tag');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropForeign('address_FK');
        });
        Schema::table('company', function (Blueprint $table) {
            $table->dropForeign('address_Comp_FK');
            $table->dropForeign('AM_FK');
        });
        Schema::table('haveTags', function (Blueprint $table) {
            $table->dropForeign('company_FK');
            $table->dropForeign('tags_FK');
        });
    }

}
