<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('address', function (Blueprint $table) {
            $table->string('street', 200)->nullable()->change();
            $table->string('zip', 10)->nullable()->change();
            $table->string('city', 100)->nullable()->change();
            $table->string('country', 100)->nullable()->change();
            $table->softDeletes();
        });
        Schema::table('company', function (Blueprint $table) {            
            $table->unsignedInteger('accountManager')->nullable()->change();
            $table->softDeletes();
        });
        Schema::table('user', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('tag', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
