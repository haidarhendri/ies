<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailHaveSegment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haveSegmentEmail', function (Blueprint $table) 
        {
            $table->unsignedInteger('idEmail');
            $table->unsignedInteger('idSegment');
            $table->timestamps();

            $table->index('idEmail', 'idEmailIndex_haveSegmentEmail');
            $table->index('idSegment', 'idSegmentIndex_haveSegmentEmail');

            $table->foreign('idEmail', 'idEmail_FK_haveSegmentEmail')->references('id')->on('email');
            $table->foreign('idSegment', 'idSegment_FK_haveSegmentEmail')->references('id')->on('segment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('haveSegmentEmail', function (Blueprint $table) 
        {
            $table->dropForeign('idEmail_FK_haveSegmentEmail')->references('id')->on('segment');
            $table->dropForeign('idSegment_FK_haveSegmentEmail')->references('id')->on('email');

            $table->dropIndex('idSegmentIndex_haveSegmentEmail');
            $table->dropIndex('idEmailIndex_haveSegmentEmail');            
        });
        Schema::dropIfExists('haveSegmentEmail');
    }
}
