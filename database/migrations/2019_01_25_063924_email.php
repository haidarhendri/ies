<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Email extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 40);
            $table->text('body');
            $table->string('senderName', 40);
            $table->string('senderEmail', 40);
            $table->string('replyToEmail', 40);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('attachment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attachment');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('haveEmail', function (Blueprint $table) {
            $table->unsignedInteger('idEmail');
            $table->unsignedInteger('idContact');
            $table->timestamps();

            $table->index('idEmail', 'idEmailIndex_haveEmail');
            $table->index('idContact', 'idContactIndex_haveEmail');

            $table->foreign('idEmail', 'idEmail_FK_haveEmail')->references('id')->on('email');
            $table->foreign('idContact', 'idContact_FK_haveEmail')->references('id')->on('contact');
        });
        Schema::create('haveAttachment', function (Blueprint $table) {
            $table->unsignedInteger('idEmail');
            $table->unsignedInteger('idAttachment');
            $table->timestamps();

            $table->index('idEmail', 'idEmailIndex_haveAttachment');
            $table->index('idAttachment', 'idAttachmentIndex_haveAttachment');

            $table->foreign('idEmail', 'idEmail_FK_haveAttachment')->references('id')->on('email');
            $table->foreign('idAttachment', 'idAttachment_FK_haveAttachment')->references('id')->on('attachment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('haveAttachment', function (Blueprint $table) {
            $table->dropForeign('idEmail_FK_haveAttachment')->references('id')->on('email');
            $table->dropForeign('idAttachment_FK_haveAttachment')->references('id')->on('attachment');
            
            $table->dropIndex('idEmailIndex_haveAttachment');
            $table->dropIndex('idAttachmentIndex_haveAttachment');
        });

        Schema::dropIfExists('haveAttachment');
        Schema::dropIfExists('attachment');
        Schema::dropIfExists('email');
    }
}
