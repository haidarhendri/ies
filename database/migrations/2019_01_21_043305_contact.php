<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactTags', function (Blueprint $table) {
            $table->string('name');
            $table->primary('name', 'tagName');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',10);
            $table->string('firstName', 25);
            $table->string('lastName', 25)->nullable();
            $table->string('email', 50);
            $table->string('phone', 15);
            $table->string('handPhone', 15);
            $table->string('jobTitle', 25);
            $table->unsignedInteger('idCompany')->nullable();
            $table->unsignedInteger('idAddress')->nullable();
            $table->string('website', 50);
            $table->string('birthday',50);
            $table->unsignedInteger('accountManager')->nullable();
            $table->binary('picture')->nullable();
            $table->text('customData')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('idCompany', 'companyContact');
            $table->index('idAddress', 'addressIndexContact');
            $table->index('accountManager', 'AMIndexContact');

            $table->foreign('idCompany', 'company_FK_Contact')->references('id')->on('company');
            $table->foreign('idAddress', 'address_FK_Contact')->references('id')->on('address');
            $table->foreign('accountManager', 'AM_FK_Contact')->references('id')->on('user');
        });
        DB::statement("ALTER TABLE contact MODIFY picture LONGBLOB");

        Schema::create('haveTagsContact', function (Blueprint $table) {
            $table->unsignedInteger('idContact');
            $table->string('tagName');
            $table->timestamps();

            $table->index('idContact', 'contactIndex');
            $table->index('tagName', 'tagNameIndex');

            $table->foreign('idContact', 'FK_Contact')->references('id')->on('contact');
            $table->foreign('tagName', 'FK_Tags')->references('name')->on('contactTags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->dropForeign('company_FK_Contact');
            $table->dropForeign('address_FK_Contact');
            $table->dropForeign('AM_FK_Contact');
        });

        Schema::table('haveTagsContact', function (Blueprint $table) {
            $table->dropForeign('FK_Contact');
            $table->dropForeign('FK_Tags');

            $table->dropIndex('contactIndex');
            $table->dropIndex('tagNameIndex');            
        });

        Schema::dropIfExists('contactTags');
        Schema::dropIfExists('contact');
        Schema::dropIfExists('haveTagsContact');
    }
}
