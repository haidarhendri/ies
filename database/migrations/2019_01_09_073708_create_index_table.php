<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->index('idAddress', 'addressIndex');
        });
        Schema::table('company', function (Blueprint $table) {
            $table->index('idAddress', 'addressIndex');
            $table->index('accountManager', 'AMIndex');
        });
        Schema::table('haveTags', function (Blueprint $table) {
            $table->primary(['idCompany', 'tagName'], 'haveTags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropIndex('addressIndex');
        });
        Schema::table('company', function (Blueprint $table) {
            $table->dropIndex('addressIndex');
            $table->dropIndex('AMIndex');
        });
        Schema::table('haveTags', function (Blueprint $table) {
            $table->dropPrimary('haveTags');
        });
    }
}
