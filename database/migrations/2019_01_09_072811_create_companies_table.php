<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('website', 100);
            $table->unsignedInteger('idAddress')->nullable();
            $table->string('phone', 15);
            $table->string('email', 50);
            $table->unsignedInteger('accountManager');
            $table->binary('picture')->nullable();
            $table->timestamps();
        });
        DB::statement("ALTER TABLE company MODIFY picture LONGBLOB");
        Schema::create('tag', function (Blueprint $table) {
            $table->string('name');
            $table->primary('name', 'tagName');
            $table->timestamps();
        });
        Schema::create('haveTags', function (Blueprint $table) {
            $table->unsignedInteger('idCompany');
            $table->string('tagName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
        Schema::dropIfExists('tag');
        Schema::dropIfExists('haveTags');
    }
}
