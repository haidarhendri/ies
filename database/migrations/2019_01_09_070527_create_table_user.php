<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('email', 50);
            $table->unsignedInteger('idAddress')->nullable();
            $table->string('phone', 15);
            $table->binary('picture')->nullable();
            $table->timestamps();
        });
        DB::statement("ALTER TABLE user MODIFY picture LONGBLOB");
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('street', 200);
            $table->string('zip', 10);
            $table->string('city', 100);
            $table->string('country', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
        Schema::dropIfExists('address');
        Schema::dropIfExists('city');
        Schema::dropIfExists('country');
    }
}
