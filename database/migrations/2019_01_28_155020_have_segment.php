<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HaveSegment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('haveEmail', function (Blueprint $table) {
            $table->dropForeign('idEmail_FK_haveEmail')->references('id')->on('email');
            $table->dropForeign('idContact_FK_haveEmail')->references('id')->on('contact');

            $table->dropIndex('idEmailIndex_haveEmail');
            $table->dropIndex('idContactIndex_haveEmail');            
        });
        Schema::dropIfExists('haveEmail');
        Schema::create('haveSegment', function (Blueprint $table) {
            $table->unsignedInteger('idContact');
            $table->unsignedInteger('idSegment');
            $table->timestamps();

            $table->index('idContact', 'idContactIndex_haveSegment');
            $table->index('idSegment', 'idSegmentIndex_haveSegment');

            $table->foreign('idContact', 'idContact_FK_haveSegment')->references('id')->on('contact');
            $table->foreign('idSegment', 'idSegment_FK_haveSegment')->references('id')->on('segment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('haveSegment', function (Blueprint $table) {
            $table->dropForeign('idContact_FK_haveSegment')->references('id')->on('segment');
            $table->dropForeign('idSegment_FK_haveSegment')->references('id')->on('contact');

            $table->dropIndex('idSegmentIndex_haveSegment');
            $table->dropIndex('idContactIndex_haveSegment');            
        });
        Schema::dropIfExists('haveSegment');
    }
}
