<?php

/*
 * This file is part of Laravel Hashids.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Hashids Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'salt' => '4toggeiutsoojobltyllvdwjxwtn80awr00lmaberwrl7mg9mvmo1fjjtf2hwrco32obgcqjcz34c0efyhpjfcpg2gu6l65ylq1fr49sb37b74fe6lx35i0kukistjj322hfnsuhi6u3dfffraox8gaur3ligj81k5o26q84skk87zntferlgbuvqlilxza633aima3qr3gqq72wr6fqdn51qhakyr4vqfcyczaf0css63rdq13uhhoxkn89ujch',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'workspace' => [
            'salt' => 'ykzwgpdqjwpjkmqms2jy3x7bzukam6ybq39l8dljzud8ielonziiymj92o8hzr60myk27i8s04wermp1pvx5d4r7bjmb8029s9piwujaf3g467t8heq1uuval346hd3s5s40lguvl7oohmdit3iu16ty9s8bt4yzztbxb2uh0sftygq1g7thoo6vxsofrpsfqo1yjzzzm0ef3tuzklcur3ba64haqqmvfpzj4yo9wghtv44ruxnxdph46l1h1qq2',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'team' => [
            'salt' => '79o0lt0dis8tmbbadyyddcmsdhmn9ryceb0yrgr1ym3gvdahhs3vpfl63jsq7okpksivt7lqw0i1c8qap1s5ebqdvpyqxj98yk8lh189fjmus336xv77g4eiohh5yei2my6zygbpdivzsgl72w12tfes5osyycu9xrjy6iquzxll8vmtgl55disonolh70295cn2j9aqv7v8pujhksfu567tgg0o69h1gmpvo7l09i4cqovfs2s9sup0r6fn6rsq',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'project' => [
            'salt' => '2urdofao9z2zj5pxicsvueigs79773fgmi6pysdp3zqsdsmwaq45kxroq7it47sk1alzep2xfxnzzt3qw79sycldb4hkk80i4lfsmpi4ahoyxnmaqr92tppzcdx59t7csoms23fvzo1h3iq91g2isqwlbdsumlf8qld34ekidmr61nt4qasds0jzkxeauqmw1rjorge5c3is7ke9kddf8byu16yf0qvhdn96u9bv4k145aic3ncievsy1leujc8b',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'campaign' => [
            'salt' => 'o6k370tylgwrz1h0thef7yzztd26vtayd06kqkmmz5cvfey1quky5ivfxrwrvnjo79can8nf05gr5oisq7u193slfej6fynn7fnqg4tnkcaufxuhtzck1nhtyzgrqnbyfyxis17vr8og832z7bexchzjku4wvx3q9y8dglzxgv2dperdxq9q48pagq0nwuseszrvqndbecyja5035svshdhiby156q6agyilj1yz301mt1w1ybda3sc7r50pelta',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'invitation' => [
            'salt' => 'cynb1xau145ym9kjvchvy2rk9r7asqaax49fb50hbddvserk93wvyb271ixlitsu1y7mzne2fb46wg0121jtsx0ejv9ii840hsx85q4u46kvkm93dnoigjpyodpkqpc9hcw6h8m6zyl2lkv8nvoebu2cu2dgivtxtho32ogz1q57xwqm5kplilmp5n9xk750danhadeg3mk5lzucw8tly4whttnq3welg93r07esyh8hrz6yylc2a2bioeys7dd1',
            'length' => 28,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'campaign_template' => [
            'salt' => 'wbw5ywcgwljryiusj7ubuwsdkjc42jjzrkeaxypmvo7m8szg7cfphktg8kxe0pfjr8hsscvgqmyl77v7myoyb8kct3lqkfmulh9j8n27h1noewb5chwyhmvsqfxt9y2b7huwaxivaz51gxvbl6lakdsxsmbynjcwha1fsk9ai5s0fazzyhwjttyjkm5yon9ucr4c39cbfwwrw54billjfwrumnsgz06l7hkadixl8gb9yy94pgr9xw5exwsswisn',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'user' => [
            'salt' => 'hfc7p8y7qu0wkevbia6c5wchd5596vsqt8eolts435wc4qploeguv0vanqg9jz2t89nk9ufoh25yx2meu82uah8ov46p5fiwmk5g01b1zm8oybr57hy1l5fhwipideqyiip67bu2nwmqu45xv7pjhyuq5xaltqfw6988qv9w3gwj96dwolaud683y8xd61b12knb6xnyz2dh246fk51bz1kcm14qeu24s0q515bimiu1bu975dy4kur1b61jl835',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'minisite' => [
            'salt' => 'mx9kj1omo4x9tq5k9zznoc4najwktxjdmxqe918fuy72xqfzyz4ymena14n70cfulmyd9ngjfbl1cb4yimyiox63oywgkjep6hnnldeiwuy9lw83j4kwpef30z4b1wilgucat851v2b44xpatp5q96rvp0lcmr78zqq2s5wwfsqrv1w2p731hf6wu5spyt438zxes6vtrhza0ez5uy50r1m62wu9f9z448w6zt7bk9qj2cfuznpcg8t1jk4i5f7z',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'widget' => [
            'salt' => '3nr1rd57kxwrqmqmlygihho7ze1ngg6jxkjabjv4bhhmi19n9s800hugbe2er10alxjo0u8t6necn0a7sh09wzbf7nensq8oqfd75hsxnuaj13g588uuytbpxnjluymghk67zwkexqzov8df84xd93kn6n9jqjl74903ksgvdk8xlo6i9tcl0a1dw428gyfwg48auhb7s006hodyq0c5t7dl2tfgaydjsyyvwlfc4ajg0bs6mum0g42faz9i19ym',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'integration' => [
            'salt' => 'wzxzhhv3i3dg4sgngtmj5ixubsn4feyqz6979x0sti4avheh62zm4kt7zmbl9600vgs8uxqeefflz7yvyxchwcf84i5dq9yxd5frqeqhett0un4zcvkdqczuejy8fff8yj3wo7tjtq77ytqpoke8arls4e0e5ox09nwl1byibhwj7jq6g9qa3nsnpkabatutpxld9xy9440fnnpstwya4utjdsww1ijl3s6kxkwzjq58x0wrik7wcyhsllhyn3lk',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ],

        'file' => [
            'salt' => 'agicfulped8zfwx8c5p4lmwiteglltuvrcpt3fbk2iwiqd2gpd4si2rhvd1n06zurnkzdv7zgi820nxcrq7tfevsckjdfgo35ouuc9yrfifi1vndy0eje6ae0y7o9d9kxlss0s3nlmfrhljea4ufq2zvhpv0sllpulyw4fy026phfjv1yvyec7qzas68wg41c1a2ral9c11ojv38wzrtyai5i2dyj3jayzhu0dgz7t4142fxt6iw76zww3xnmy66',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyz0123456789'
        ]

    ],

];
