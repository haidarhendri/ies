<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi minimal harus enam karakter dan sesuai dengan konfirmasi password.',
    'reset' => 'Kata sandi anda sudah berhasil diatur ulang!',
    'sent' => 'Kami telah mengirimkan anda email untuk mengatur ulang password!',
    'token' => 'Token untuk mengatur ulang kata sandi sudah tidak berlaku.',
    'user' => 'Kami tidak dapat menemukan pangguna dengan email tersebut.',

];
