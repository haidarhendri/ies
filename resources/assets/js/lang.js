import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueCookie from 'vue-cookie'
import Locale from './vue-i18n-locales.generated'

Vue.use(VueI18n)
Vue.use(VueCookie)

// Create VueI18n instance with options
export const i18n = new VueI18n({
  locale: Vue.cookie.get('locale') ? Vue.cookie.get('locale') : 'en',
  messages: Locale
})
