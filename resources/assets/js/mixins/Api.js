import {user} from './Api/User.js'
import {company} from './Api/Company.js'
import {contact} from './Api/Contact.js'
import {segment} from './Api/Segment.js'
import {email} from './Api/Email.js'
import {image} from './Api/Image.js'


export default{
	user,
	company,
	email,
	segment,
	contact,
	image
}