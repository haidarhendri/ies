export const email = {
	created(){
		console.log('Hello from mixin email')
	},
  	methods: {
    	emailList(){
    		return axios.get('/email/get-list-segment')
    	},
    	deleteEmail(data){
    		return  axios.post('/email/delete', data)
    	},
    	addEmail(data){
    		return axios.post('/email/to-segment',
	          data,{
	          headers: {
	              'Content-Type': 'multipart/form-data'
	            }
	          }
	      	)
    	},
        saveEmail(data){
            return axios.post('/email/to-segment-store',
              data,{
              headers: {
                  'Content-Type': 'multipart/form-data'
                }
              }
            )
        }	
  	}
}