export default [
  {
    'id': 1,
    'name': 'Noelia O\'Kon',
    'email': 'otho.smitham@example.com',
    'userLevel': 'Administrator',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 2,
    'name': 'Mr. Enid Von PhD',
    'email': 'pollich.cecilia@example.com',
    'userLevel': 'User',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 3,
    'name': 'Colton Koch',
    'email': 'little.myrna@example.net',
    'userLevel': 'User',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 4,
    'name': 'Gregory Vandervort',
    'email': 'dock47@example.org',
    'userLevel': 'User',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 5,
    'name': 'Miss Rahsaan Heaney IV',
    'email': 'ugrady@example.org',
    'userLevel': 'User',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 6,
    'name': 'Ms. Crystel Zemlak IV',
    'email': 'amari.rau@example.com',
    'userLevel': 'Administrator',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 7,
    'name': 'Nona McDermott',
    'email': 'adrien.baumbach@example.org',
    'userLevel': 'Administrator',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 8,
    'name': 'Miss Genoveva Murazik V',
    'email': 'ohettinger@example.net',
    'userLevel': 'Administrator',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 9,
    'name': 'Beulah Huels',
    'email': 'stefan99@example.com',
    'userLevel': 'User',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 10,
    'name': 'Zoe Klein',
    'email': 'ejacobson@example.org',
    'userLevel': 'Administrator',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 11,
    'name': 'Noelia O\'Kon',
    'email': 'otho.smitham@example.com',
    'userLevel': 'Administrator',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 12,
    'name': 'Mr. Enid Von PhD',
    'email': 'pollich.cecilia@example.com',
    'userLevel': 'User',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 13,
    'name': 'Colton Koch',
    'email': 'little.myrna@example.net',
    'userLevel': 'User',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 14,
    'name': 'Gregory Vandervort',
    'email': 'dock47@example.org',
    'userLevel': 'User',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 15,
    'name': 'Miss Rahsaan Heaney IV',
    'email': 'ugrady@example.org',
    'userLevel': 'User',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 16,
    'name': 'Ms. Crystel Zemlak IV',
    'email': 'amari.rau@example.com',
    'userLevel': 'Administrator',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 17,
    'name': 'Nona McDermott',
    'email': 'adrien.baumbach@example.org',
    'userLevel': 'Administrator',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 18,
    'name': 'Miss Genoveva Murazik V',
    'email': 'ohettinger@example.net',
    'userLevel': 'Administrator',
    'isActive': 'no',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 19,
    'name': 'Beulah Huels',
    'email': 'stefan99@example.com',
    'userLevel': 'User',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  },
  {
    'id': 20,
    'name': 'Zoe Klein',
    'email': 'ejacobson@example.org',
    'userLevel': 'Administrator',
    'isActive': 'yes',
    'lastLoggedIn': '30 Days'
  }
]
