import Vue from 'vue'
import './pollyfills'
import './bootstrap'
import VueRouter from 'vue-router'
import VueNotify from 'vue-notifyjs'
import VeeValidate from 'vee-validate'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import VueInternationalization from 'vue-i18n'
import Locale from './vue-i18n-locales.generated'
import App from './App.vue'
import ElementUI from 'element-ui';

// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import SideBar from './components/UIComponents/SidebarPlugin'

// Router
import routes from './routes/routes'

// Library Imports
import './assets/sass/paper-dashboard.scss'
import './assets/sass/element_variables.scss'
import './assets/sass/demo.scss'
import 'flag-icon-css/css/flag-icon.css'

import sidebarLinks from './sidebarLinks'

require('froala-editor/js/froala_editor.pkgd.min')
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

import VueFroala from 'vue-froala-wysiwyg'
import Swatches from 'vue-swatches'
import "vue-swatches/dist/vue-swatches.min.css"
import * as Sentry from '@sentry/browser'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSave, faEdit, faFileImport, faStar, faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faSave, faEdit, faFileImport, faStar, faThumbsUp)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// Plugin Setup
Vue.use(VueFroala)
Vue.use(ElementUI)

// Plugin Setup
Vue.use(VueRouter)
Vue.use(GlobalDirectives)
Vue.use(GlobalComponents)
Vue.use(VueNotify)
Vue.use(SideBar, {sidebarLinks: sidebarLinks})
Vue.use(VeeValidate, {
  events: 'change',
  fieldsBagName: 'fieldsValidate'
})
Vue.use(ElementUI, { lang })
locale.use(lang)
Vue.use(VueInternationalization)
Vue.component(
  'swatches',
  Swatches
)

Sentry.init({
  dsn: saassone.sentry,
  integrations: [new Sentry.Integrations.Vue({ Vue })]
})

const i18n = new VueInternationalization({
  locale: Vue.cookie.get('locale') ? Vue.cookie.get('locale') : 'en',
  silentTranslationWarn: true,
  messages: Locale
})

// Configure Router
const router = new VueRouter({
  mode: 'history',
  routes,
  linkActiveClass: 'active'
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  render: h => h(App),
  router,
  i18n,
}).$mount('#app')
