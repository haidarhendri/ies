const state = {
    filter: []
};

const getters = {
    getFilter : state => {
        return state.filter;
    }
};

const mutations = {
    SET_FILTER : (state, playload) => {
        state.filter = playload;
    },
    PUSH_FILTER : (state, playload) => {
        state.filter.push(playload)
    }
};

const actions = {
    SET_FILTER : (context, playload) => {
        context.commit('SET_FILTER', playload)
    },
    ADD_FILTER : (context, playload) => {
        context.commit('PUSH_FILTER', playload)
    }
};


export default {
    state,
    getters,
    actions,
    mutations,
};