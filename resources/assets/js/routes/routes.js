import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'

import Overview from '../components/Dashboard/Overview.vue'
import Form from '../components/Dashboard/Views/Form.vue'
import CreateUserForm from '../components/Dashboard/Views/CreateUserForm.vue'
import CreateCompanyForm from '../components/Dashboard/Views/CreateCompanyForm.vue'
import CreateContactForm from '../components/Dashboard/Views/CreateContactForm.vue'
import UserList from '../components/Dashboard/Views/UserList.vue'
import CompanyList from '../components/Dashboard/Views/CompanyList.vue'
import Contact from '../components/Dashboard/Views/Contact.vue'
import ContactList from '../components/Dashboard/Views/ContactList.vue'
import ContactFormEdit from '../components/Dashboard/Views/Contact/ContactFormEdit.vue'
import SegmentContainer from '../components/Dashboard/Views/SegmentContainer.vue'
import EmailContainer from '../components/Dashboard/Views/EmailContainer.vue'
import EmailList from '../components/Dashboard/Views/EmailList.vue'
import SegmentList from '../components/Dashboard/Views/SegmentList.vue'
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

let main = {
  path: '/',
  component: DashboardLayout,
  redirect: '/dashboard',
  children: [
    {
      path: 'dashboard',
      name: 'Dashboard',
      component: Overview
    },
    {
      path: 'formUser',
      name: 'Create User',
      component: CreateUserForm
    },
    {
      path: 'formCompany',
      name: 'Create Company',
      component: CreateCompanyForm
    },
    {
      path: 'userList',
      name: 'User List',
      component: UserList
    }
    ,
    {
      path: 'companyList',
      name: 'Company List',
      component: CompanyList
    }
    ,
    {
      path: 'formContact',
      name: 'Create Contact',
      component: CreateContactForm
    }
    ,
    {
      path: 'contact/',
      name: 'Contact List',
      component: Contact,
      redirect: '/contact/list',
      children:[
        {
          path: 'edit/:id',
          name: 'Edit Contact',
          component:ContactFormEdit
        },
        {
          path: 'list',
          name: 'Contact List',
          component:ContactList
        }
      ]
    }
    ,
    {
      path: 'segments',
      name: 'Segments',
      component: SegmentContainer
    }
    ,
    {
      path: 'segmentList',
      name: 'Segment List',
      component: SegmentList
    }
    ,
    {
      path: 'email',
      name: 'Email',
      component: EmailContainer
    }
    ,
    {
      path: 'emailList',
      name: 'Email List',
      component: EmailList
    }
  ]
}

let notFound = {
  path: '*',
  component: NotFound
}

const routes = [
  main,
  notFound
]






/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
