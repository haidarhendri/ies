import { i18n } from './lang'

let navigations = [
  {
    name: i18n.t('menu.dashboard'),
    icon: 'ti-panel',
    path: '/dashboard'
  }
];

export default navigations
