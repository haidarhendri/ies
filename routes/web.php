<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('user','API\UserController@store');
Route::post('user/get-id','API\UserController@getId');
Route::post('user/delete','API\UserController@destroy');
Route::get('user/get-list','API\UserController@list');

Route::post('company','API\CompanyController@store');
Route::post('company/get-id','API\CompanyController@getId');
Route::post('company/delete','API\CompanyController@destroy');
Route::get('company/get-list','API\CompanyController@list');

Route::post('contact','API\ContactController@store');
Route::post('contact/get-id','API\ContactController@getId');
Route::post('contact/delete','API\ContactController@destroy');
Route::get('contact/get-list','API\ContactController@list');

Route::post('address','API\AddressController@store');
Route::post('address/get-id','API\AddressController@getId');
Route::get('address/get-list','API\AddressController@list');

Route::post('tag','API\TagController@store');
Route::post('tag/get-name','API\TagController@getTagName');
Route::get('tag/get-list','API\TagController@list');

Route::post('segment','API\SegmentController@store');
Route::post('segment/get-id','API\SegmentController@getId');
Route::post('segment/delete','API\SegmentController@destroy');
Route::get('segment/get-list','API\SegmentController@list');

Route::post('email','API\EmailController@store');
Route::post('email/get-id','API\EmailController@getId');
Route::post('email/delete','API\EmailController@destroy');
Route::post('email/to-segment','API\EmailController@sendToSegment');
Route::post('email/to-segment-store','API\EmailController@saveSegmentEmail');
Route::post('email/to-contact','API\EmailController@sendToContact');
Route::get('email/get-list','API\EmailController@list');
Route::get('email/get-list-segment','API\EmailController@getSegmentEmail');

Route::post('haveTags','API\HaveTagsController@store');
Route::post('haveTags/get-data','API\HaveTagsController@getData');
Route::get('haveTags/get-list','API\HaveTagsController@list');

Route::post('haveTagsContact','API\HaveTagsContactController@store');
Route::post('haveTagsContact/get-data','API\HaveTagsContactController@getData');
Route::get('haveTagsContact/get-list','API\HaveTagsContactController@list');

Route::get('/upload', function () {
    return view('upload');
});

Route::post('/upload_file','API\EmailController@store');

Route::post('/attachmentFile','API\AttachmentController@store');

Route::get('{any?}', 'AppController@index')->where('any', '^(?!(api|xyz).*$).*');
