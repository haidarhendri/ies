let mix = require('laravel-mix');
let webpack = require('webpack');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
  resolve: {
    alias: {
      videojs: 'video.js',
      WaveSurfer: 'wavesurfer.js',
      '@': path.resolve(__dirname, 'resources/assets/js/components')
    }
  },
  plugins: [
    new webpack.EnvironmentPlugin(['APP_URL']),
     new webpack.NormalModuleReplacementPlugin(/element-ui[\/\\]lib[\/\\]locale[\/\\]lang[\/\\]zh-CN/, 'element-ui/lib/locale/lang/en')
  ],
});

// The below code will inject i18n Kazupon/vue-18-loader as a loader for .vue files.
mix.extend('i18n', function(webpackConfig, ...args) {
  webpackConfig.module.rules.forEach((module) => {
    // Search for the "vue-loader" component, which handles .vue files.
    if (module.loader !== 'vue-loader') {
      return;
    }

    // Within this module, add the vue-i18n-loader for the i18n tag.
    module.options.loaders.i18n = '@kazupon/vue-i18n-loader';
  });
});

mix.i18n().js('resources/assets/js/app.js', 'public/js');
